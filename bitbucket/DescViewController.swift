//
//  DescViewController.swift
//  bitbucket
//
//  Created by apple on 1/30/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class DescViewController: UIViewController {

    @IBOutlet weak var itemDescImageView: UIImageView!
    
    @IBOutlet weak var itemDescNameLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    var img:UIImage?
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.cornerRadius = 7
        self.itemDescImageView.image = img
        self.itemDescNameLabel.text = name
    }
    

    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
