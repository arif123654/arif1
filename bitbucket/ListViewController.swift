//
//  ListViewController.swift
//  bitbucket
//
//  Created by apple on 1/30/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var name = ["Pasta","Fruits","food","Biriani","Berger"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    

   

}

extension ListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ListTableViewCell
       
        
        cell.itemImageView.image = UIImage(named: name[indexPath.row])
        cell.itemNameLabel.text = name[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "DescViewController") as! DescViewController
        
        vc.img = UIImage(named: name[indexPath.row])
        vc.name = name[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
